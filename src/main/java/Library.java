import dao.BookDao;
import dao.BookDaoSqlite;
import domain.Book;

public class Library {
    private BookDao bookDao;

    public BookDao getBookDao() {
        return bookDao;
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public static void main(String[] args) {
        Library library = new Library();
        library.setBookDao(new BookDaoSqlite());



        library.getBookDao().addBook(new Book("Jan", "Kowalski", 453));

    }


}

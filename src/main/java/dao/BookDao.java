package dao;

import domain.Book;

import java.util.List;

public interface BookDao {
    void addBook(Book book);
    void removeBook(Book book);
    List<Book> findAll();
}

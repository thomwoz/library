package dao;

import domain.Book;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


public class BookDaoSqlite implements BookDao{
    private Connection connection;

    public BookDaoSqlite() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:library.db");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        createTable();
    }

    private void createTable() {
        String sql = "CREATE TABLE IF NOT EXISTS Books("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "title TEXT NOT NULL, "
                + "author TEXT NOT NULL, "
                + "pages INTEGER DEFAULT 0"
                + ")";

//

        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addBook(Book book) {
        String sql = "INSERT INTO Books (id, title, author, pages) VALUES ("
                + book.getId() +","
                + "\'"+book.getTitle()+"\'" + ", "
                + "\'"+book.getAuthor()+"\'" + ", "
                + "\'"+book.getPages()+"\'"
                + ")";


        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void removeBook(Book book) {

        String sql = "delete from Books where id=" + book.getId();


        try {
            Statement s = connection.createStatement();
            s.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public List<Book> findAll() {
        return null;
    }
}
